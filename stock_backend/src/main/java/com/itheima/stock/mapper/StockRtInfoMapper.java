package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.Stock4EvrDayDomain;
import com.itheima.stock.common.domain.Stock4EvrWeekDomain;
import com.itheima.stock.common.domain.Stock4MinuteDomain;
import com.itheima.stock.common.domain.StockUpdownDomain;
import com.itheima.stock.pojo.StockRtInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author jack
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2022-03-09 11:05:14
* @Entity com.itheima.stock.pojo.StockRtInfo
*/
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);
    /**
     * 沪深两市个股涨幅分时行情数据查询，以时间顺序和涨幅查询前10条数据
     * @param curTime 当前股票有效时间
     * @return
     */
    List<StockUpdownDomain> stockIncreaseLimit(@Param("curTime") Date curTime);

    /**
     * 根据时间和涨幅降序排序全表查询
     * @return
     */
    List<StockUpdownDomain> stockAll();
    /**
     * 统计指定日期内涨停或者跌停的数据
     * 日期范围不能失效，否则分库分表查询失效
     * @param curTime 股票交易时间
     * @param openDate 对应的开盘日期
     * @param flag 1：涨停 0：跌停
     * @return
     */
    List<Map> upDownCount(@Param("avlDate") Date curTime, @Param("openDate") Date openDate, @Param("flag") Integer flag);


    /**
     * 统计指定时间点下，各个涨跌区间内股票的个数
     * @param avlDate
     * @return
     */
    List<Map> stockUpDownScopeCount(@Param("avlDate") Date avlDate);

    /**
     * 查询指定股票在指定日期下的每分钟的成交流水信息
     * @param code 股票编码
     * @param avlDate 最近的股票有效交易日期
     * @return
     */
    List<Stock4MinuteDomain> stockScreenTimeSharing(@Param("stockCode") String code, @Param("startDate") Date avlDate, @Param("endtDate") Date endDate);
    /**
     *  统计指定股票在指定日期范围内的每天交易数据统计
     * @param code 股票编码
     * @param beginDate 前推的日期时间
     * @return
     */
    List<Stock4EvrDayDomain> stockCreenDkLine(@Param("stockCode") String code, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

    /**
     * 批量插入功能
     * @param stockRtInfoList
     */
    int insertBatch(List<StockRtInfo> stockRtInfoList);


    List<Stock4EvrWeekDomain> stockCreenWkLine(@Param("stockCode") String code, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
