package com.itheima.stock.mapper;

import com.itheima.stock.common.domain.*;
import com.itheima.stock.pojo.StockBusiness;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
* @author jack
* @description 针对表【stock_business(主营业务表)】的数据库操作Mapper
* @createDate 2022-03-09 11:05:14
* @Entity com.itheima.stock.pojo.StockBusiness
*/
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);

    int updateByPrimaryKey(StockBusiness record);

    List<StockBusiness> getAll();

    /**
     * 获取所有股票的code
     * @return
     */
    List<String> getStockIds();

    //模糊查询

    List<FindDomain> getAllByBusinessAndBusiness(@Param("code")String code);

    //显示股票的详细信息

    GuDetail getDetail(@Param("gcode")String code);

//个股最新分时行情数据接口分析 seconDetailDomain

    seconDetailDomain seconDetailDomain(@Param("scode")String code,@Param("curDate") Date curDate);


    List<secondDomain> getsecondData(@Param("scode") String code, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
