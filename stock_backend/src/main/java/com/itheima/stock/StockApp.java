package com.itheima.stock;

import com.itheima.stock.common.domain.StockInfoConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author by jack
 * @Date 2021/12/29
 * @Description 定义main启动类
 */

@EnableAspectJAutoProxy(exposeProxy = true)
@EnableTransactionManagement
@SpringBootApplication
@MapperScan("com.itheima.stock.mapper")
@EnableConfigurationProperties(StockInfoConfig.class) ////开启常用参数配置bean    开启配置 初始化(stock)并加入ioc容器
@ServletComponentScan//开启WebFilter、WebServlet、WebListener配置支持
public class StockApp {
    public static void main(String[] args) {
        SpringApplication.run(StockApp.class,args);
    }




}