package com.itheima.stock.study.day03;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/*
    Stream流的收集操作 : 第三部分

    1 创建一个ArrayList集合，并添加以下字符串。字符串中前面是姓名，后面是年龄
        "zhangsan,23"
        "lisi,24"
        "wangwu,25"
    2 保留年龄大于等于24岁的人，并将结果收集到Map集合中，姓名为键，年龄为值

    收集方法 :
    public static  Collector toMap(Function keyMapper  ,   Function valueMapper)：把元素收集到Map集合中
 */
public class StreamDemo7 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("zhangsan,23");
        list.add("lisi,24");
        list.add("wangwu,25");

        // collect 只负责收集数据
        // Collectors.toMap负责在底层创建集合对象 , 添加元素
        // toMap方法中的s就是代表的是集合中的每一个元素
        // 第一个参数 : 如何获取map集合中的键
        // 第二个参数 : 如何获取map集合中的值   需要两个参数 第一个健 第二个值
        Map<String, String> map = list.stream().filter(s -> Integer.parseInt(s.split(",")[1]) > 23).collect(
                Collectors.toMap(
                        (String s) -> {
                            return s.split(",")[0];
                        }
                        ,
                        (String s) -> {
                            return s.split(",")[1];
                        }
                )
        );
        System.out.println(map);
    }

    @Test
    public void test01(){
        HashMap<String, String> hm = new HashMap<>();
        hm.put("11", "曹植");
        hm.put("22", "曹丕");
        hm.put("33", "曹熊");
        hm.put("44", "曹冲");
        hm.put("22", "曹昂");
        //将map中的键转换成int类型，封装到集合下
        //Set<Integer> intSet = hm.keySet().stream().map(keyStr -> {return Integer.valueOf(keyStr);}).collect(Collectors.toSet());
       //只对健进行过滤???
        Set<Integer> intSet = hm.keySet().stream().map(keyStr -> Integer.valueOf(keyStr)).collect(Collectors.toSet());

        System.out.println(intSet );
//           结果  22 11 33 44
//// Java 8
//        List<String> collect = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());
//        System.out.println(collect); //[A, B, C, D]
//
//        // Extra, streams apply to any data type.
//        List<Integer> num = Arrays.asList(1,2,3,4,5);
//        List<Integer> collect1 = num.stream().map(n -> n * 2).collect(Collectors.toList());
//        System.out.println(collect1); //[2, 4, 6, 8, 10]



//        //将map中的键转化成int类，并获取一个等于22的值（出现多个，则任意获取一个）
//        Optional<Integer> first = hm.keySet().stream().map(keyStr -> Integer.valueOf(keyStr)).filter(num -> num == 29).findFirst();
//        //判断是否存在
//        if (first.isPresent()) {
//            System.out.println(first.get());
//        }else {
//            System.out.println("数据不存在");
//        }

        Optional<Integer> first = hm.keySet().stream().map(keyStr -> Integer.valueOf(keyStr)).filter(num -> num == 29).findFirst();


        Optional<Integer> any = hm.keySet().stream().map(keyStr -> Integer.valueOf(keyStr)).filter(num -> num == 22).findAny();
        if (any.isPresent()) {
            System.out.println(any.get());
        }
        //注意：在串行的流中，findAny和findFirst返回的，都是第一个对象；而在并行的流中，findAny返回的是最快处理完的那个线程的数据
    }

}