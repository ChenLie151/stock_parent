package com.itheima.stock.study.day03;
/*
   复习  Lambda表达式体验 :
 */
public class LambdaDemo {
    public static void main(String[] args) {
        //传统匿名内部类
goSwimming(new Swimming() {
    @Override
    public void swim() {
        System.out.println("重写了接口内部的方法");
    }
});
        System.out.println("下面是lamda表达式-----------------");
goSwimming(()->{
    System.out.println("这里是lamda表达式");
});

    }


    //        普通方法
    public static void goSwimming(Swimming swimming) {
        swimming.swim();
    }

//    - lambda表达式可以理解为对匿名内部类的一种简化 , 但是本质是有区别的
//- 面向对象思想 :
//            - 强调的是用对象去完成某些功能
//- 函数式编程思想 :
//            - 强调的是结果 , 而不是怎么去做
//



}
