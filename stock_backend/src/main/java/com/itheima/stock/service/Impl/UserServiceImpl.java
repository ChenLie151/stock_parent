package com.itheima.stock.service.Impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Strings;
import com.itheima.stock.common.enums.ResponseCode;
import com.itheima.stock.mapper.SysRoleMapper;
import com.itheima.stock.mapper.SysUserMapper;
import com.itheima.stock.pojo.SysRole;
import com.itheima.stock.pojo.SysUser;
import com.itheima.stock.pojo.RoleID;
import com.itheima.stock.service.UserService;
import com.itheima.stock.utils.IdWorker;
import com.itheima.stock.utils.RedisUtil;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.req.LoginZHReqVo;
import com.itheima.stock.vo.req.UpRoleVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author by itheima
 * @Date 2021/12/30
 * @Description 定义服务接口实现
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 分布式环境保证生成id唯一
     */
    @Autowired
    private IdWorker idWorker;

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysRoleMapper SysRoleMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisUtil redisUtil;//拿到工具类



    @Override
    public R<LoginRespVo> login(LoginReqVo vo) {
        if (vo==null || Strings.isNullOrEmpty(vo.getUsername()) || Strings.isNullOrEmpty(vo.getPassword())){
            return R.error(ResponseCode.DATA_ERROR.getMessage());
        }

        //从redis中拿到数据
        String rcode = (String)redisUtil.get(vo.getRkey());
        System.out.println(rcode+"redis拿到的");

        String rcode2 = (String)redisUtil.get(vo.getRkey());
        System.out.println("尝试第二次拿到丑的"+rcode2);
//校验
        if (Strings.isNullOrEmpty(rcode) || !rcode.equals(vo.getCode())) {
            return R.error(ResponseCode.DATA_ERROR.getMessage());
        }

        //根据用户名查询用户信息
        SysUser user=this.sysUserMapper.findByUserName(vo.getUsername());
        //判断用户是否存在，存在则密码校验比对
        if (user==null || !passwordEncoder.matches(vo.getPassword(),user.getPassword())){
            return R.error(ResponseCode.SYSTEM_PASSWORD_ERROR.getMessage());
        }
        //组装登录成功数据
        LoginRespVo respVo = new LoginRespVo();
        //属性名称与类型必须相同，否则copy不到
        BeanUtils.copyProperties(user,respVo);
        return  R.ok(respVo);
    }
    @Override
    public R<Map> generateCaptcha() {
        //1.生成4位数字验证码
        String checkCode = RandomStringUtils.randomNumeric(4);
        //2.获取全局唯一id
        String rkey=String.valueOf(idWorker.nextId());
        //验证码存入redis中，并设置有效期1分钟
        redisTemplate.opsForValue().set(rkey,checkCode,60, TimeUnit.SECONDS);
        //3.组装数据

        HashMap<String, String> map = new HashMap<>();
        map.put("rkey",rkey);
        map.put("code",checkCode);
        return R.ok(map);  // 一个成功的结果 验证码
    }

    @Override
    public SysUser querybyname(String name) {
        SysUser byUserName = sysUserMapper.findByUserName(name);

        return byUserName;
    }

    @Override
    public R<PageResult> Zhusers(LoginZHReqVo loginZHReqVo) {
        System.out.println("进入service"+"是否为空");
        //1.设置分页参数
        PageHelper.startPage(Integer.parseInt(loginZHReqVo.getPageNum()),Integer.parseInt(loginZHReqVo.getPageSize()));
        //2.通过mapper查询
        List<SysUser> zhusers = sysUserMapper.Zhusers();
        System.out.println("进入service"+zhusers.isEmpty()+"是否为空");
        //3.封装到PageResult下
        //3.1 封装PageInfo对象
        PageInfo<SysUser> listPageInfo = new PageInfo<SysUser>(zhusers);
        //3.2 将PageInfo转PageResult          前端不需要那么多参数 所以这里进行 部分获取 截取
        PageResult<SysUser> pageResult = new PageResult<>(listPageInfo);
        //4.封装R响应对象
        int sum = sysUserMapper.findSum();
        String s = String.valueOf(sum);
        int i = Integer.parseInt(loginZHReqVo.getPageSize());
        pageResult.setTotalPages(sum);
        pageResult.setPageSize(i);
        pageResult.setPageNum(Integer.parseInt(loginZHReqVo.getPageNum()));
        return R.ok(pageResult);
    }

    @Override
    public void addUser(SysUser sysUser) {
                // 添加唯一ID
        long l = idWorker.nextId();
        String s = String.valueOf(l);

        Date T = new Date();

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String format = simpleDateFormat.format(T);
    sysUser.setCreateTime(T);
        sysUser.setId(s);
        System.out.println("添加id 时间后"+sysUser.toString());
        sysUserMapper.addUser(sysUser);

    }
    @Override
    public List<String> rolesManage(String userId) {
        List<String> roleid = SysRoleMapper.findRoleid(userId);
        System.out.println("应该查到的rid"+roleid.toString());
        return roleid;
    }
    //公共角色信息
    @Override
    public List<SysRole> findAllrole(){
        List<SysRole> findallrole = SysRoleMapper.findallrole();
        return findallrole;
    }
    @Override   //角色更新
    public void updateRole(UpRoleVo upRoleVo) {
        List<RoleID> lrid = new ArrayList<>();
        String userId = upRoleVo.getUserId();
        //角色id
        List<String> roleIds = upRoleVo.getRoleIds();
//存放idwork产生数据的集合
        List<Long> idw = new ArrayList<>();
        //数量和 角色数量一致   一个角色信息对象一个 特有id
        for (int i = 0; i < roleIds.size(); i++) {
            long l = idWorker.nextId();
            idw.add(l);
        }
//遍历角色id
        for (int i = 0; i < roleIds.size(); i++) {
            RoleID r = new RoleID();
            r.setRid(roleIds.get(i));
            lrid.add(r);
        }
  //遍历idwork ids
        for (int i = 0; i < idw.size(); i++) {
            RoleID roleID = lrid.get(i);
            roleID.setWid(idw.get(i));
        }
        Date d = new Date();  // 数据产生时间
//  向向中间表删除数据
        SysRoleMapper.deletRole(userId);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = df.format(d);
//Map<String,Object> optionMap = new HashMap<>();
//                     optionMap.put("roleIds",roleIds);   //角色id
//                     optionMap.put("idw",idw);              //存放idwork产生数据的集合
        if (!roleIds.isEmpty()){
            SysRoleMapper.updateRole(userId,lrid,format);
        }
    }

    //删除用户
    @Override

    public void deletUser(Long id) {
       sysUserMapper.deletUser(id);  //删除用户表
int i = 10 / 0;
        sysUserMapper.deletRu(id);    //删除角色表
    }


    @Override
    public void deletRu(Long id) {
        sysUserMapper.deletRu(id);
    }


//事务管理员
@Transactional(
        isolation = Isolation.DEFAULT,
        readOnly = false,
        timeout = 10,
        propagation = Propagation.REQUIRED
)
@Override
public void swManage(Long id) {
this.deletUser(id);

}


}