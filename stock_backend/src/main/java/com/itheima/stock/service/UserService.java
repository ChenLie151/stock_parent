package com.itheima.stock.service;

import com.itheima.stock.common.domain.InnerMarketDomain;
import com.itheima.stock.pojo.SysRole;
import com.itheima.stock.pojo.SysUser;
import com.itheima.stock.pojo.User;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.req.LoginZHReqVo;
import com.itheima.stock.vo.req.UpRoleVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;
import com.itheima.stock.vo.resp.roleResult;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author by itheima
 * @Date 2021/12/30
 * @Description 用户服务
 */
public interface UserService {
    /**
     * 用户登录功能实现
     * @param vo
     * @return
     */
    R<LoginRespVo> login(LoginReqVo vo); //返回一个类型的结果
    /**
     * 生成验证码
     *  map结构：
     *      code： xxx,
     *      rkey: xxx
     * @return
     */
    R<Map> generateCaptcha();

        SysUser querybyname(String name);

        R<PageResult>  Zhusers(LoginZHReqVo loginZHReqVo);

      void addUser(SysUser sysUser) ;
      //根据 用户id 查询 对应角色 id

    List<String> rolesManage(String userId);


    public List<SysRole> findAllrole();

    public void  updateRole(UpRoleVo upRoleVo);



    public void  deletUser(Long id);


    public void deletRu(Long id);

    @Transactional(
            isolation = Isolation.DEFAULT,
            readOnly = false,
            timeout = 10,
            propagation = Propagation.REQUIRED
    )
    public  void  swManage(Long id);
}
