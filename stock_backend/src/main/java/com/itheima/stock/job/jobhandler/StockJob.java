package com.itheima.stock.job.jobhandler;


import com.itheima.stock.service.StockTimerService;
import com.itheima.stock.service.StockTimerTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 定义股票相关数据的定时任务
 * @author laofang
 */
@Component
public class StockJob {
    @Autowired
    private StockTimerTaskService stockTimerTaskService;
    @Autowired
    private StockTimerService stockTimerService1;


    /**
     * 获取大盘数据
     */
    private static Logger logger = LoggerFactory.getLogger(StockJob.class);
int i = 0;

    /**
     * 定义定时任务，采集国内大盘数据
     */
    @XxlJob("getStockInnerMarketInfos")
    public void getStockInnerMarketInfos(){
        System.out.println("查询内盘");


        stockTimerTaskService.getInnerMarketInfo();
    }



    @XxlJob("getStockOuterMarketInfos")
    public void getStockOuterMarketInfos(){
        System.out.println("查询外盘");
        stockTimerTaskService.getOuterMarketInfo();

    }

    /**
     * 定时采集A股数据
     */

    @XxlJob("hema_job_test")
    public void jobTest(){
        System.out.println("jobTest run.....");
        System.out.println("我被访问啦啦啦");
        System.out.println(i);

//获取A股信息
        //    几个股票  盘块信息
//        System.out.println("下面执行获取大盘数据任务");
//        List<String> stockIds = stockTimerService.getStockIds();
//        System.out.println(stockIds.toString());
        //板块
        i++;
stockTimerService1.getStockSectorRtIndex();



    //    stockTimerService.getInnerMarketInfo();

    }

    @XxlJob("hema_job_test1")
    public void jobTest1(){
        System.out.println("jobTest1 run.....");

        System.out.println(i);

//获取A股信息
        //    几个股票  盘块信息
//        System.out.println("下面执行获取大盘数据任务");
//        List<String> stockIds = stockTimerService.getStockIds();
//        System.out.println(stockIds.toString());
        //板块
        i++;
        stockTimerService1.getStockRtIndex();
        System.out.println("具体`股票`插入");


        //    stockTimerService.getInnerMarketInfo();

    }




    public void init(){
        logger.info("init");
    }

    public void destroy(){
        logger.info("destory");
    }


}
