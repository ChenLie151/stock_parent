package com.itheima.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;

//redis的配置
@Configuration      //表明是配置类
public class RedisConfig {
    //编写配置信息
     @Bean
    public RedisTemplate<String,Object> redisTemplate (RedisConnectionFactory redisConnectionFactory) throws UnknownHostException{


    RedisTemplate<String,Object> template =new RedisTemplate<>();
         template.setConnectionFactory(redisConnectionFactory);
         //可以配置具体的序列化方式
              //默认的Key序列化器为：JdkSerializationRedisSerializer         设置新序列化
         template.setKeySerializer(new StringRedisSerializer());
         template.setHashKeySerializer(new StringRedisSerializer());
//也可以对value进行特定序列化

    return template;


}
}
