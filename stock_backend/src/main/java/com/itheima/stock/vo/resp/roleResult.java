package com.itheima.stock.vo.resp;

import com.github.pagehelper.PageInfo;
import com.itheima.stock.pojo.SysRole;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 分页工具类
 */
@Data
public class roleResult<T> implements Serializable {
    /**
     * 总记录数
     */
    private List<String> ownRoleIds;


    private List<SysRole> allRole;


}


